package com.sgiasia.javaspringboot.springmvc.mvcdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name,
                           @RequestParam(name="ucapan", required=false, defaultValue="Pagi") String ucapan,
                           Model model) {

        model.addAttribute("name", name);
        model.addAttribute("ucapan", ucapan);

        return "greeting";
    }
}